 
//Librerias
import processing.serial.*;
import grafica.*;// Interfaz grafica
public GPlot plot2,plot3,plot4;
int contadorx = 0, contadory=0, contadorz =0;
 
//Creo objetos

Serial myPort;        
 
//Variables globales
Serial port;
float x=0;
float y=0;
float z = 0;
int bt=0;  // para leer x bt=0, para leer y bt =1, para leer z bt=2
PrintWriter output; // Para guardar datos en un fichero
 
//Funciones dibujatitulo, Interfaz y Salir
 

 
 
void setup(){
  myPort = new Serial(this,Serial.list()[0], 9600);  
  output = createWriter("datos_Acelerometro.txt"); 
 size(1200, 650);
  GPointsArray points2 = new GPointsArray(100);
  GPointsArray points3 = new GPointsArray(100);
  GPointsArray points4 = new GPointsArray(100);
   
  // Setup for the second plot
  plot2 = new GPlot(this);
  plot3 = new GPlot(this);
  plot4 = new GPlot(this);
  plot2.setPos(new float[] {0, 0});
  plot3.setPos(new float[]{0,210});
  plot4.setPos(new float[]{0,420});
  plot2.setDim(new float[] {width-100,100});
  plot3.setDim(new float[] {width-100,100});
  plot4.setDim(new float[]{width-100,100});
  
  plot2.getXAxis().getAxisLabel().setText("X");
  plot2.getYAxis().getAxisLabel().setText("Y");
  plot2.getTitle().setText("Acelerometro MMA7455");
  plot2.setPoints(points2);
  plot2.activateZooming();//
  
  plot3.getXAxis().getAxisLabel().setText("X");
  plot3.getYAxis().getAxisLabel().setText("Y");
  plot3.getTitle().setText("Acelerometro MMA7455");
  plot3.setPoints(points3);
  plot3.activateZooming();
  
  plot4.getXAxis().getAxisLabel().setText("X");
  plot4.getYAxis().getAxisLabel().setText("Y");
  plot4.getTitle().setText("Acelerometro MMA7455");
  plot4.setPoints(points4);
  plot4.activateZooming(); 
}
 
void serialEvent (Serial myPort) 
{ GPointsArray points2 = plot2.getPoints();
  GPointsArray points3 = plot3.getPoints();
  GPointsArray points4 = plot4.getPoints();
 String inString = myPort.readStringUntil('\n');
 if (inString != null) { // Si hay un dato en el puerto
 inString = trim(inString); //Se hace la asignación
  
  switch(bt){
    case 0 :
    contadorx+=10;
     x = float(inString); // Se asigna el primer valor leido
      if(points2.getNPoints() == 0){
    points2.add(x, contadorx, "(" + str(x) + " , " + str(contadorx) + ")");
    plot2.setPoints(points2);
  }
  else{
    GPoint lastPoint = points2.getLastPoint();
     
    if(!lastPoint.isValid() || sq(lastPoint.getX() - x) + sq(lastPoint.getY() + contadorx) > 2500){
      points2.add(x,contadorx, "(" + str(x) + " , " + str(contadorx) + ")");
      plot2.setPoints(points2);
    }
  }
  if(keyPressed && key == ' '){
    plot2.setPoints(new GPointsArray(100));
  }
     bt=1; //Se lee el próximo byte
     output.print("Coordenada x : " + x + TAB);  //Se guarda el eje x en un fichero
     break;
   case 1 :
   contadory+=10;
    y = float(inString);
 if(points3.getNPoints() == 0){
    points3.add(y, contadory, "(" + str(y) + " , " + str(contadory) + ")");
    plot3.setPoints(points3);
  }
  else{
    GPoint lastPoint = points3.getLastPoint();
     
    if(!lastPoint.isValid() || sq(lastPoint.getX() - y) + sq(lastPoint.getY() + contadory) > 2500){
      points3.add(y,contadory, "(" + str(y) + " , " + str(contadory) + ")");
      plot3.setPoints(points3);
    }
  }
   
  // Reset the points if the user pressed the space bar
  if(keyPressed && key == ' '){
    plot3.setPoints(new GPointsArray(100));
  }
    bt=2; //el próximo byte que leeré será humedad
    output.print("Coordenada y : "+y+TAB); //Guardo en fichero
    break;
   case 2 :   
     contadorz +=10;   
     z = float(inString);
   if(points4.getNPoints() == 0){
    points4.add(z, contadorz,"(" + str(z) + " , " + str(contadorz) + ")");
    plot4.setPoints(points4);
  }
  else{
    GPoint lastPoint = points4.getLastPoint();
     
    if(!lastPoint.isValid() || sq(lastPoint.getX()- z) + sq(lastPoint.getY() + contadorz) > 2500){
      points4.add(z,contadorz, "(" + str(z) + " , " + str(contadorz) + ")");
      plot4.setPoints(points4);
    }
  }
   
  // Reset the points if the user pressed the space bar
  if(keyPressed && key == ' '){
    plot4.setPoints(new GPointsArray(100));
  }
     bt=0;
     output.print("Coordenada z : "+z);
     output.println(""); //Salto de línea
     break;
     
   
  }
     
 
 }
}
 
void draw(){
  background(0);
   plot2.beginDraw();
    plot2.drawBackground();
    plot2.drawBox();
    plot2.drawXAxis();
    plot2.drawYAxis();
    plot2.drawTitle();
    plot2.drawGridLines(GPlot.BOTH);
    plot2.drawLines();
    plot2.drawPoints();
    plot2.endDraw();//
    plot3.beginDraw();
    plot3.drawBackground();
    plot3.drawBox();
    plot3.drawXAxis();
    plot3.drawYAxis();
    plot3.drawTitle();
    plot3.drawGridLines(GPlot.BOTH);
    plot3.drawLines();
    plot3.drawPoints();
    plot3.endDraw();//
    plot4.beginDraw();
    plot4.drawBackground();
    plot4.drawBox();
    plot4.drawXAxis();
    plot4.drawYAxis();
    plot4.drawTitle();
    plot4.drawGridLines(GPlot.BOTH);
    plot4.drawLines();
    plot4.drawPoints();
    plot4.endDraw();
 
}
