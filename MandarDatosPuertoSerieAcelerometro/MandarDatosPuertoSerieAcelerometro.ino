#include <Wire.h> //Librería Wire
#include <MMA_7455.h> //Librería MMA7455

MMA_7455 mySensor = MMA_7455(); // Instancia a la librería

char xVal, yVal, zVal; 

void setup()
{
  Serial.begin(9600);
  // Set the sensitivity you want to use
  // 2 = 2g, 4 = 4g, 8 = 8g
  mySensor.initSensitivity(2);
  // Calibrate the Offset, that values corespond in 
  // flat position to: xVal = -30, yVal = -20, zVal = +20
  // !!!Activate this after having the first values read out!!!
  //mySensor.calibrateOffset(5, 20, -68);
}

void loop()
{
  xVal = mySensor.readAxis('x'); //Read out the 'x' Axis
  yVal = mySensor.readAxis('y'); //Read out the 'y' Axis
  zVal = mySensor.readAxis('z'); //Read out the 'z' Axis
  //Serial.print("Valor de X : ");
  
  Serial.print(xVal, DEC);
  Serial.print("\n");
  Serial.print(yVal, DEC);
  Serial.print("\n");
  Serial.print(zVal, DEC);
  Serial.print("\n");
  
  //delay(1000);
}
